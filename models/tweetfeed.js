var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var TwitterfeedSchema = new Schema(
  {
    tweetText: {type: String},
    tweetID: {type: String, required: true},
    retweets: {type: Number},
    favorites: {type: Number},
    mentions: {type: Array},
    tweetURL: {type: String},
    username: {type: String},
    createdat: {type: String},
  }
);

var Twitterfeed = mongoose.model('Twitterfeed', TwitterfeedSchema);
module.exports = Twitterfeed;