var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../api');
var should = chai.should();
var config = require('../config.js');

chai.use(chaiHttp);


describe('Tweets', function() {
it('should list ALL tweets on /api/get_all_tweets GET', function(done) {
  chai.request('http://localhost:8081')
    .get('/api/get_all_tweets')
    .end(function(err, res){
      res.should.have.status(200);
      res.should.be.json;
      res.body.should.be.a('array');
      res.body[0].should.have.property('_id');
      res.body[0].should.have.property('retweets');
      res.body[0].should.have.property('tweetText');
      done();
    });
});
it('should add Tweets on /api/save_tweets POST', function(done) {
  chai.request('http://localhost:8081')
    .post('/api/save_tweets')
    .send(config.testing_post_tweet)
    .end(function(err, res){
      res.should.have.status(200);
      res.should.be.json;
      res.body.should.be.a('object');
      res.body.should.have.property('status');
      res.body.status.should.equal('success');
      done();
    });
});
});