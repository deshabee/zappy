# Installation
### The machine should have Nodejs and mongodb installed and running.
### Install npm
```sh 
$ sudo npm install
```

### Install The used packages
```sh 
$ npm install
```

### How to run tests
```sh 
$ mocha
```

### How to run the system
in case of running the project outside dockers we need to change the mongodb connection paramter to be localhost instead of mongo:27017 , available in the config.js and then:
```sh 
$ nodejs index.js
$ nodejs api.js
```
in case of running the project using the docker image we need the mongodb connection paramter to be mongo:27017 , available in the config.js and then:

```sh 
$ sudo docker-compose up  
```