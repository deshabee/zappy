module.exports = {
	save_tweets : function(tweets,res){
		var mongoose = require('mongoose');
		var Tweetfeed = require('./models/tweetfeed');
		var config = require('./config.js')
		mongoose.connect(config.mongodbconnectionlocal);
		var db = mongoose.connection;
		db.on('error', console.error.bind(console, 'connection error:'));
		db.once('open', function() {
			// console.log(tweets);
			tweets.forEach(function(tweetbody){
		  		totalmentions = new Array();
		  		mentions = tweetbody.entities.user_mentions
		  		text = tweetbody.text
		  		tweetID = tweetbody.id_str
		  		retweets = tweetbody.retweet_count
		  		favorites = tweetbody.favorite_count
		  		username = tweetbody.user.screen_name
		  		tweetURL =  `https://twitter.com/${username}/status/${tweetID}`
		  		createdat = tweetbody.created_at
		  		createdat = createdat.split(" +");
		  		createdat = createdat[0]
		  		if(Object.keys(mentions).length != 0){
		  			for(mention of mentions){
		  				singlemention = {}
		  				singlemention.mentioned_name = mention.screen_name
		  				singlemention.account_id = mention.id
		  				totalmentions.push(singlemention)
		  			}
		  		}
				var tweet = {tweetID:tweetID}
  				Tweetfeed.findOneAndUpdate(tweet, {tweetText:text , username:username , createdat : createdat , tweetID:tweetID , retweets:retweets , favorites:favorites ,mentions:totalmentions ,tweetURL:tweetURL} , {upsert: true}, function(err){res.status(500).send({'status':'failed'})})
  				res.status(200).send({'status':'success'})
  			})
		});
}
};