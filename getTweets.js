module.exports = {
	get_all_tweets : function(res){
		var mongoose = require('mongoose');
		var config = require('./config.js')
		mongoose.connect(config.mongodbconnectionlocal);
		var db = mongoose.connection;
		var Tweetfeed = require('./models/tweetfeed');
		db.on('error', console.error.bind(console, 'connection error:'));
		db.once('open', function() {
			Tweetfeed.find(function (err, tweets) {
				if (err) return res.send(500).send(err);
				return res.status(200).send(tweets)
			})
		})
	}
}