var RtmClient = require('@slack/client').RtmClient;
var CLIENT_EVENTS = require('@slack/client').CLIENT_EVENTS;
var RTM_EVENTS = require('@slack/client').RTM_EVENTS;
var config = require('./config.js');

api_token = config.api_token
channel_name = config.channel_name
trigger_msg = config.trigger_msg

var rtm = new RtmClient(api_token);
rtm.start();

let channel;
rtm.on(CLIENT_EVENTS.RTM.AUTHENTICATED, (rtmStartData) => {
  for (const c of rtmStartData.channels) {
      if (c.is_member && c.name ===channel_name) { channel = c.id }
  }
  console.log(`Logged in as ` + rtmStartData.self.name + ` of team ` + rtmStartData.team.name);
});

rtm.on(CLIENT_EVENTS.RTM.RTM_CONNECTION_OPENED, function () {
  // rtm.sendMessage("Hello!", channel);
  console.log(`connection to ${channel} is now opened`)
});

rtm.on(RTM_EVENTS.MESSAGE, function(message) {
    // if(message.)
    if(message.text.indexOf(trigger_msg) > -1){
    	var twitterObj = require('./twitter_module.js')
      twitterObj.get_account_feed();
    }
});

