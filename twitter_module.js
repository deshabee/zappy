module.exports = {
  get_account_feed : function(){
    var Twitter = require('twitter');
    var config = require('./config.js');

    var client = new Twitter({
      consumer_key: config.consumer_key,
      consumer_secret: config.consumer_secret,
      access_token_key: config.access_token_key,
      access_token_secret: config.access_token_secret
    });
    var params = {screen_name: config.following_account};

    client.get('statuses/home_timeline', params, function(error, tweets, response) {
      if (!error) {
       responseBody = JSON.parse(response.body)
       // var dbObject = require('./saveTweets.js')
       // dbObject.save_tweets(responseBody)
       var request = require('request');
       request.post(
        'http://localhost:8081/api/save_tweets',
        { json: responseBody },
        function (error, response, body) {
          if (!error && response.statusCode == 200) {
            console.log(body)
          }
        }
        );
     }
     else{
       console.log(error)
     }
   });

  }
};
